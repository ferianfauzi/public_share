<!-- Footer -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="one-third column">
                <h4>BEM Amikom.</h4>
                <p>Selalu ada untuk memperjuangkan aspirasi mahasiswa.<br/>
                    Saatnya kita beraksi dan menjadi agen perubahan.</p>
            </div>
            <div class="one-third column">
                <?php
                $enak = "select * from datastatis where nama like '%visi%'";
                $telan = mysql_query($enak);
                $no = 1;
                while ($value = mysql_fetch_array($telan)) { ?>
                <h4><?php echo $value['nama'];?>.</h4>
                <p><?php echo substr($value['isi'],140,166)?> ... <a href="index.php?menu=datastatis&id=<?php echo $value['id_datastatis'];?>">baca selanjutnya>></a></p>
                
                <?php }
                ?>
            </div>
            <div class="one-third column">
                <h4>Galeri.</h4>
                <div class="flicko">
                    <ul>
                        <?php
                        $enak = "select * from galeri order by id_galeri desc limit 4";
                        $telan = mysql_query($enak);
                        $no = 1;
                        while ($value = mysql_fetch_array($telan)) {
                            ?>
                            <li>
                                <a href="index.php?menu=galeri"><img width="50px" height="50px" src="admin/upload/thumbs/galeri/<?php echo $value['foto']; ?>" alt="upload"></a>
                            </li>
<?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Copyright -->
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="eight columns copy-info">
                    <p>&copy; 2014 BEM Amikom. All rights reserved.<br/>
                        Supported by <a href="http://amikom.ac.id" target="_blank">STMIK Amikom Yogyakarta</a>.</p>
                </div>
                <div class="eight columns copy-social">
                    <ul class="social-icons">

                        <li class="twitter"><a href="#"></a></li>
                        <li class="facebook"><a href="#"></a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Boxed Wrapper -->
</div>

</body>
</html>