-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 14 Jan 2015 pada 18.26
-- Versi Server: 5.5.32
-- Versi PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `bem`
--
CREATE DATABASE IF NOT EXISTS `bem` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bem`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(25) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'agus', 'fdf169558242ee051cca1479770ebac3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
  `id_agenda` int(10) NOT NULL AUTO_INCREMENT,
  `nama_agenda` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `tempat` varchar(30) NOT NULL,
  `tujuan` varchar(30) NOT NULL,
  `penanggung_jawab` varchar(30) NOT NULL,
  PRIMARY KEY (`id_agenda`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `nama_agenda`, `tanggal`, `tempat`, `tujuan`, `penanggung_jawab`) VALUES
(1, 'Study Tour Bali', '2014-12-31', 'Bali, Lombok', 'KKL', 'Prof M Suyanto'),
(2, 'Pelantikan Ketua BEM ', '2015-01-18', 'Gedung Merbabu, Jalan Magelang', 'Pelantikan pengurus baru', 'Bapak Negae Surogai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id_album` int(10) NOT NULL AUTO_INCREMENT,
  `nama_album` varchar(30) NOT NULL,
  PRIMARY KEY (`id_album`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `album`
--

INSERT INTO `album` (`id_album`, `nama_album`) VALUES
(3, 'Kemahasiswaan'),
(4, 'Inagurasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE IF NOT EXISTS `anggota` (
  `id_anggota` int(10) NOT NULL AUTO_INCREMENT,
  `id_devisi` int(10) NOT NULL,
  `nama_anggota` varchar(30) NOT NULL,
  `foto` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  PRIMARY KEY (`id_anggota`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `id_devisi`, `nama_anggota`, `foto`, `alamat`, `no_telp`) VALUES
(3, 249, 'Joko Susanto', '16122014_26514_bagus-kodok.jpg', 'Yogyakarta', '084673733'),
(4, 251, 'agus haryanto sudirman', '16122014_25602_sf.jpg', 'Di Ponggol\r\n', '085746382354'),
(6, 249, 'Siska Anomali', '16122014_25952_9521.jpg', 'dimana mana ada', '3534545'),
(7, 249, 'Robertino Pungli Dimana', '16122014_17763_9529.jpg', 'ada disitu tuh lho', '32435454'),
(8, 249, 'Miska Anteve', '16122014_16324_9477.jpg', 'Studio 5 Antv Jakarta Barat', '3423454'),
(9, 250, 'Amburegul Ameseyu', '16122014_2333_picture004.jpg', 'ada disana sih tuh lho', '454656');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspirasi`
--

CREATE TABLE IF NOT EXISTS `aspirasi` (
  `id_aspirasi` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(35) NOT NULL,
  `isi` text NOT NULL,
  `status` enum('aktif','non-aktif') DEFAULT 'non-aktif',
  `jawaban` varchar(500) NOT NULL DEFAULT 'belum dibalas',
  PRIMARY KEY (`id_aspirasi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `aspirasi`
--

INSERT INTO `aspirasi` (`id_aspirasi`, `email`, `isi`, `status`, `jawaban`) VALUES
(2, 'joko@gmail.com', 'seharusnya bem bisa menjadi panutan, bukan hanya membuat acara tanpa kejelasan dana dan lain sebagainya.', 'aktif', 'Terima kasih banyak joko@gmail.com atas perhatian dan sarannya, nantinya akan kami berikan seluruh jiwa dan raga kami untuk istri dan suami kami *nyambung gak sih ?'),
(3, 'askirun@gmail.com', 'Saya menemui ada beberapa dosen yang kurang berkompeten, mohon bantuannya!', 'aktif', 'Silahkan segera komunikasikan secara langsung melalui kesekretariatan kepada kami, dan bawa bukti serta list ketidak kompetenannya. Terima Kasih.'),
(4, 'mbahsap@gmail.com', 'Menggunakan waktu dengan tanpa bertanggung jawab adalah yang sering dilakukan oleh mahasiswa, bagaimana bem tidak bisa memberikan solusi yang solutif ?\r\n', 'aktif', 'Iya mas maaf kami belum bisa memberikan sebuah terobosan baru, namun ini akan segara kami perbaiki kedepannya.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datastatis`
--

CREATE TABLE IF NOT EXISTS `datastatis` (
  `id_datastatis` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) NOT NULL,
  `isi` text NOT NULL,
  PRIMARY KEY (`id_datastatis`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `datastatis`
--

INSERT INTO `datastatis` (`id_datastatis`, `nama`, `isi`) VALUES
(5, 'Visi Misi', '<div style="font-size: 13.3333330154419px;"><font face="Arial, Verdana">Visi:&nbsp;</font></div><div style="font-size: 13.3333330154419px;"><font face="Arial, Verdana">Menjadi salah satu organisasi unggulan dalam pengajaran, pendidikan, penelitian, dan pengabdian pada bidang kemahasiswaan yang berdasarkan pada spiritualitas, intelektualitas, dan profesionalitas.&nbsp;</font></div><div style="font-size: 13.3333330154419px;"><font face="Arial, Verdana"><br></font></div><div style="font-size: 13.3333330154419px;"><div><font face="Arial, Verdana">Misi:&nbsp;</font></div><div><font face="Arial, Verdana">1. Mengembangkan sistem kaderisasi yang melahirkan pemimpin yang berintelektual dan berakhlak mulia dengan wawasan luas.&nbsp;</font></div><div><font face="Arial, Verdana">2. Melaksanakan program pengajaran dan pendidikan non formal dalam bidang kemahasiswaan yang berdasarkan pada karakter unggulan.&nbsp;</font></div><div><font face="Arial, Verdana">3. Melakukan penelitian dalam bidang kemahasiswaan yang berwawasan global agar menjadi sumber informasi bagi civitas akademika.&nbsp;</font></div><div><font face="Arial, Verdana">4. Menjalankan program yang memberikan kemanfaatan luas dalam bidang sosial masyarakat&nbsp;</font></div><div><font face="Arial, Verdana">5. Membangun kerjasama dengan seluruh civitas akademik di lingkungan kampus agar tercipta harmoni yang berkesinambungan&nbsp;</font></div><div><font face="Arial, Verdana">6. Mendasarkan seluruh program kerja BEM dengan menjunjung tinggi nilai spiritualitas, intelektualitas, dan profesionalitas.</font></div><div><font face="Arial, Verdana"><br></font></div></div>'),
(6, 'Tentang Kami', '<div><font face="Arial, Verdana"><span style="font-size: 13.3333330154419px;">Setiap individu pasti meliliki cinta. Dalam konteks ini cinta yang dimaksud adalah kecintaan terhadap sesuatau (passion). Kecintaan ini idealnya bisa menjadi ruh potensi dan kreativitas mahasiswa yang akan didesikasikan untuk kemajuan Amikom dan Indonesia. Namun sekarang potensi dan kreatifitas ini belum bisa menjadi wujud kontibusi. Maka dari itu kita ingin menghadirkan BEM KM sebagai wadah yang bisa merangkai kecintaan tersebut.Karena cinta â€“ cinta ini ketika dirangkai akan menjadi energi yang sangat besar dalam melandasi karya-karya besar ke depan.</span></font></div><div><font face="Arial, Verdana"><span style="font-size: 13.3333330154419px;"><br></span></font></div><div><font face="Arial, Verdana"><span style="font-size: 13.3333330154419px;">Sebuah karya akan besar jika dilandasi dengan cinta. Cinta ibarat sebuah energi yang akan mengarahkan kemanakah karya ini akan diberikan..</span></font></div>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `devisi`
--

CREATE TABLE IF NOT EXISTS `devisi` (
  `id_devisi` int(10) NOT NULL AUTO_INCREMENT,
  `nama_devisi` varchar(25) NOT NULL,
  `tugas` varchar(100) NOT NULL,
  PRIMARY KEY (`id_devisi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252 ;

--
-- Dumping data untuk tabel `devisi`
--

INSERT INTO `devisi` (`id_devisi`, `nama_devisi`, `tugas`) VALUES
(249, 'Keislaman', 'mengontrol kegiatan keagamaan yang ada'),
(250, 'Ilmu Pengetahuan', 'Memastikan kualitas anggota'),
(251, 'Presiden Mahasiswa', 'coba');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
  `id_galeri` int(10) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `id_album` int(11) NOT NULL,
  PRIMARY KEY (`id_galeri`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `foto`, `keterangan`, `id_album`) VALUES
(3, '16122014_15950_uyo.jpg', 'Ketua BEM', 4),
(4, '16122014_31599_Asekkk,,.jpg', 'Kebahagiaan bersama', 3),
(5, '16122014_24591_ss.jpg', 'Bareng bareng', 3),
(6, '16122014_5754_kalisuci 2.jpg', 'Kebersamaan', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE IF NOT EXISTS `kontak` (
  `id_kontak` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(30) NOT NULL,
  `fun_page` varchar(25) NOT NULL,
  PRIMARY KEY (`id_kontak`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id_kontak`, `email`, `alamat`, `no_telp`, `fun_page`) VALUES
(2, 'bemamikom@amikom.ac.', 'Grha STMIK AMIKOM Yogyakarta  \r\nJl. Ringroad Utara Condong Catur Depok Sleman Yogyakarta', '( 0274 ) 884201', 'http://fb.me/kmamikom');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil`
--

CREATE TABLE IF NOT EXISTS `profil` (
  `id_profil` int(25) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `tahun_berdiri` int(10) NOT NULL,
  `sejarah` text NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_profil`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `profil`
--

INSERT INTO `profil` (`id_profil`, `nama`, `tahun_berdiri`, `sejarah`, `deskripsi`) VALUES
(9, 'ONEGAI', 2010, 'Merupakan sebuah organisasi dengan kemampuan mistis yang mampu mengubah orang menjadi gambar dan gambar menjadi basah.', 'Silahkan untuk yang berminat hubungi dokter terdekat.'),
(10, 'DJOKAMI', 2013, 'Organisasi mahasiswa dengan mahasiswa paling sedikit sepanjang masa, yaitu 0. Tidak jelas siapa yang memulai dan siapa yang mengakhiri, semuanya tidak ada yang bertanggung jawab, akhirnya semuanya tak terkendali seperti apapun juga.', 'Bertahan dari gempuran emas yang dahsyat menantang api dari gunung kilimanjaro.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
