<?php

function pesan($field) {
    $con = (isset($_GET['con'])) ? $_GET['con'] : 'none';
    switch ($con) {
        case '0':
            echo '<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Selamat!</strong> Berhasil menambah data ' . $field . ' .</div>';
            break;
        case '1':
            echo '<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Maaf !</strong> Mohon cek kembali data, dan ulangi masukkan data ' . $field . '.</div> ';
            break;
        case '2':
            echo '<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Selamat!</strong> Berhasil mengubah data ' . $field . '</div>';
            break;

        case '3':
            echo '<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Maaf !</strong>Gagal mengubah data ' . $field . '</div>';
            break;

        case '4':
            echo '<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Selamat!</strong> Berhasil menghapus data ' . $field . '</div>';
            break;
        case '5':
            echo '<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Maaf !</strong>Gagal menghapus data ' . $field . ' mungkin data bersifat primary key yang sedang digunakan tabel lain.</p></div>';
            break;
        case '6':
            echo '<div class="pesan" style="color:red;">Gagal menambah data ' . $field . ' karena No.RM sudah terdaftar</p></div>';
            break;
        case '7':
            echo '<div class="pesan" style="color: #138EDB;">Berhasil menambah data ' . $field . ' cek data kembali sebelum melanjutkan.</p></div>';
            break;
        case '8':
            echo '<div class="pesan" style="color: #138EDB;">Berhasil mengubah data ' . $field . ' cek data kembali sebelum melanjutkan.</p></div>';
            break;
        case '9':
            echo '<div class="pesan" style="color:red;">Extensi file yang didukung adalah bmp,gif,jpg,jpeg,png.</p></div>';
            break;
        case '10':
            echo '<div class="pesan" style="color:red;">Extensi file yang didukung adalah pdf.</p></div>';
            break;
        case '11':
            echo '<div class="pesan" style="color:red;">Gagal menghapus data album foto pastikan gallery yang ada di dalamnya, sudah tidak ada</p></div>';
            break;
        case '12':
            echo '<div class="pesan" style="color:red;">Gagal menambah data ' . $field . ' karena <u>tanggal atau jam agenda bentrok</u> atau sudah di pakai cek kembali</p></div>';
            break;
        case '13':
            echo '<div class="pesan" style="color:red;">Data yang anda minta berdasarkan bulan dan tahun tersebut tidak ada di database</p></div>';
            break;
        case '14':
            echo '<div class="pesan" style="color:red;">Gagal menambah data ' . $field . ' karna <u>NAMA KATEGORI</u> sudah terpakai cek kembali</p></div>';
            break;
        case '15':
            echo '<div class="pesan" style="color:red;">Gagal menambah data ' . $field . ' karna <u>JUDUL</u> sudah terpakai cek kembali</p></div>';
            break;
        case '16':
            echo '<div class="pesan" style="color:blue;">Tidak jadi edit gambar di ' . $field . '</p></div>';
            break;
        case '17':
            echo '<div class="pesan" style="color:red;">Extensi file yang didukung adalah doc,docx,ppt,pptx,pdf,xls,xlsx.</p></div>';
            break;

        default:
            break;
    }
}

?>