<?php

function tgl_indo($tgl) {
    $nama_hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
    $tanggal1 = substr($tgl, 0, 10);
    $jam1 = substr($tgl, 11, 8);
    $pecah_tanggal = explode("-", $tanggal1);
    $pecah_jam = explode(":", $jam1);

    //mktime(int hour, int min, int sec, int mon, int day, int year);
    $kembali = mktime($pecah_jam[0], $pecah_jam[1], $pecah_jam[2], $pecah_tanggal[1], $pecah_tanggal[2], $pecah_tanggal[0]);

    $hari = date("w", $kembali);
    $hr = $nama_hari[$hari];

    $tanggal = substr($tgl, 8, 2);
    $bulan = getBulan(substr($tgl, 5, 2));
    $tahun = substr($tgl, 0, 4);
    $jam = substr($tgl, 10, 6);
    return $hr . ', ' . $tanggal . ' ' . $bulan . ' ' . $tahun . ' | ' . $jam . ' WIB';
}

?>
