<div id="insider"> <!-- Page-header -->
    <div class="container page-header">
        <div class="sixteen columns">
            <h3>Galeri <strong>Foto</strong>.</h3>
            <p>Kehangatan kebersamaan dan <br> kekompakan yang tersusun rapi di album kebahagiaan.</p>
        </div>
    </div>
    <!-- Breadcrumb -->
    <div class="container">
        <div class="sixteen columns">
            <ul class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li class="active">Galeri </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <div class="container">
        <!-- Filter -->
        <div class="row">
            <div class="sixteen columns">
                <ul class="filter">
                    <li class="current all"><a href="#">Semua</a></li>
                    <?php
                    $enak = "select * from album";
                    $telan = mysql_query($enak);
                    $no = 1;
                    while ($value = mysql_fetch_array($telan)) {
                        ?>
                        <li class="<?php echo $value['nama_album']; ?>"><a href="#"><?php echo $value['nama_album']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="row">
            <div class="portfolio">
                <?php
                $enak = "select * from galeri g join album a on g.id_album=a.id_album";
                $telan = mysql_query($enak);
                $no = 1;
                while ($value = mysql_fetch_array($telan)) {
                    ?>
                    <!-- Portfolio item -->
                    <div class="four columns" data-id="id-<?php echo $no;?>" data-type="<?php echo $value['nama_album'];?>">
                        <a href="admin/upload/galeri/<?php echo $value['foto'];?>" class="fancybox">
                            <img src="admin/upload/thumbs/galeri/<?php echo $value['foto'];?>" class="image-polaroid">
                        </a>
                        <div class="item-description">
                            <!--<h4><a href="#"></a></h4>-->
                            <p><?php echo $value['keterangan'];?>.</p>
                        </div>
                    </div>
                    <!-- Portfolio item -->
                <?php 
                $no++;
                } ?>
            </div>
        </div>
    </div>
</div>