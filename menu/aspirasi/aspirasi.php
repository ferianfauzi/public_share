<div id="insider">
    <!-- Page-header -->
    <div class="container page-header">
        <div class="sixteen columns">
            <h3>Suarakan <strong>ASPIRASIMU !</strong>.</h3>
            <p>Berhenti menjadi seorang yang menunggu<br>mulailah bertindak dan menjadi pelopor!</p>
        </div>
    </div>
    <!-- Breadcrumb -->
    <div class="container">
        <div class="sixteen columns">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>

                <li class="active">Aspirasi</li>
            </ul>
        </div>
    </div>
</div>

<div class="main-wrapper">
    <div class="container">
        <div class="twelve columns">
            <?php
            if (isset($_GET['pesan'])) {
                $pesan = $_GET['pesan'];

                if ($pesan == 'berhasil') {
                    echo '<div class="success">
                <p><strong>Berhasil!</strong> Aspirasi anda telah disuarakan, tunggu dipublish!</p>
            </div>';
                } elseif( $pesan=='gagal') {
                    echo '<div class="error">
                <p><strong>Maaf !</strong> Aspirasi anda belum kami terima!</p>
            </div>';
                }
            }
            ?>

            <div class="twelve columns alpha">                	
                <form id="comment-respond" method="post" action="menu/aspirasi/kirim.php">
                    <h4>Suarakan <strong>aspirasimu</strong></h4>
                    <div id="respond-input">

                        <div>
                            <label>E-mail:</label>
                            <input type="text" name="email" value="" required="">
                        </div>

                    </div>
                    <div id="respond-textarea">
                        <div>
                            <label>Aspirasi:</label>
                            <textarea name="isi" value="" required=""></textarea>
                        </div>
                    </div>
                    <div id="respond-submit">
                        <input type="submit" class="button" value="Kirim">
                    </div>
                </form>
            </div>
            <!-- Comments lists -->
            <div class="twelve columns alpha">
                <div class="underline-heading">
                    <h4><strong>Aspirasi</strong> Terbaru</h4>
                </div>

<?php
$enak = "select * from aspirasi where status='aktif' and jawaban!='belum dibalas' order by id_aspirasi desc";
$telan = mysql_query($enak);
$no = 1;
while ($value = mysql_fetch_array($telan)) {
    ?>

                    <ol class="comment-list">
                        <li>
                            <div class="comment-area">
                                <div class="comment-avatar"><img src="img/comment-avatar.png" alt="Avatar"></div>
                                <div class="comment-description">
                                    <div class="comment-by">
                                        <strong><?php echo $value['email']; ?></strong>
                                    <!--<span class="published">September 17, 2012</span>-->
                                    </div>
                                    <p><?php echo $value['isi']; ?>.</p>
                                    <!--<a href="#" class="reply">Reply</a>-->
                                </div>
                            </div>
                            <ol class="child-list">
                                <li>
                                    <div class="comment-area">
                                        <div class="comment-avatar"><img src="img/comment-avatar.png" alt="Avatar"></div>
                                        <div class="comment-description">
                                            <div class="comment-by">
                                                <strong>Administrator</strong>
                                                <!--<span class="published">October 22, 2012</span>-->
                                            </div>
                                            <p><?php echo $value['jawaban']; ?>.</p>
                                            <!--<a href="#" class="reply">Reply</a>-->
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </li>

                    </ol>

<?php } ?>
            </div>    

            <!-- Comments respond -->

        </div>


    </div>
</div>