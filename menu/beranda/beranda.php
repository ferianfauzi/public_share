
<!-- Insider -->
<div id="insider"> <!-- Page-header -->
    <div class="container page-header text-center">
        <div class="sixteen columns">
            <h3>Badan <strong>Eksekutif</strong> Mahasiswa</h3>
            <p>Badan Eksekutif Mahasiswa STMIK Amikom Yogyakarta<br/>Bersama menumbuhkan jiwa pembelajaran yang tinggi.</p>
        </div>
    </div>
</div>

<!-- Content -->
<div class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon doc"></span>
                    <h4><strong>Penelitian</strong><br/>Ilmiah.</h4>
                    <p>Kami secara aktif mendukung penelitian ilmiah oleh mahasiswa.</p>
                </div>
            </div>
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon resize-full"></span>
                    <h4><strong>Perluasan</strong><br/>Hubungan.</h4>
                    <p>Membantu dalam komunikasi antar mahasiswa lintas kampus.</p>
                </div>
            </div>
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon picture"></span>
                    <h4><strong>Acara</strong><br/>Positif.</h4>
                    <p>Membuat acara yang bersifat positif pendukung akademik mahasiswa.</p>
                </div>
            </div>
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon support"></span>
                    <h4><strong>Dukungan</strong><br/>Hobi.</h4>
                    <p>Kami akan mendukung hobi yang positif di lingkungan kampus.</p>
                </div>
            </div>
            <div class="sixteen columns"><hr class="spacer"></div>
        </div>
    </div>

    <div class="full-width-container light text-center">        
        <div class="container">
            <div class="row">
                <div class="sixteen columns">
                    <h2>Kami siap memperjuangkan aspirasi mahasiswa.</h2>
                    <p>Apabila anda memiliki uneg-uneg, usulan, saran maupun kritik, silahkan hubungi kami.<br/>
                        Dengan mengklik link dibawah ini.</p>
                    <a href="index.php?menu=aspirasi" target="_blank" class="button">Aspirasi Mahasiswa</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">        
        <div class="row">
            <div class="eight columns">
                <div class="underline-heading">
                    <h4>Tentang Kami.</h4>
                </div>
                <p>Adapun beberapa fungsional BEM Amikom adalah sebagai berikut.</p>
                <ul>
                    <li class="code">Penyalur aspirasi.</li>
                    <li class="monitor">Pejuang sosial.</li>
                    <li class="monitor">Perencana program kegiatan.</li>
                    <li class="support">Koordinator komunikasi.</li>
                </ul>
                <a href="index.php?menu=aboutus" class="button">Selanjutnya...</a>
            </div>
            <div class="eight columns">
                <div class="underline-heading">
                    <h4>Aspirasi.</h4>
                </div>
                <?php
                $q = "select * from aspirasi order by id_aspirasi desc limit 2";
                $k = mysql_query($q);
                while ($row = mysql_fetch_array($k)) {
                    ?>
                    <blockquote>
                        <p><?php echo $row['isi'];?></p>
                        <cite><?php echo $row['email'];?></cite>
                    </blockquote>
                <?php } ?>
            </div>
            <div class="sixteen columns"><hr class="spacer"></div>
        </div>

        <div class="row">
            <div class="sixteen columns">
                <div class="underline-heading">
                    <h4><strong>Foto</strong> terbaru.</h4>
                </div>
                <p>Berikut adalah foto kegiatan BEM AMIKOM Yogyakarta yang terbaru.</p>
            </div>
            <!-- Carousel -->
            <div class="carousel touchcarousel grey-blue">       
                <div class="touchcarousel-container">
                    <!-- Item -->
                    <?php
                $q = "select * from galeri order by id_galeri desc";
                $k = mysql_query($q);
                while ($row = mysql_fetch_array($k)) {
                    ?>
                    <div class="four columns touchcarousel-item">
                        <img src="admin/upload/thumbs/galeri/<?php echo $row['foto'];?>" alt="Post" class="image-polaroid">
                        <h4><a href="#"><?php echo $row['keterangan'];?>.</a></h4>

                    </div>
                    <!-- Item -->
                   
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
