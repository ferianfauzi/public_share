<div id="insider"> <!-- Page-header -->
    <div class="container page-header">
        <div class="sixteen columns">
            <h3>Siapakah Kami</h3>
        </div>
    </div>
    <!-- Breadcrumb -->
    <div class="container">
        <div class="sixteen columns">
            <ul class="breadcrumb">
                <li><a href="index.php">Beranda</a></li>
                <li class="active">Tentang Kami</li>
            </ul>
        </div>
    </div>
</div>

<div class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="sixteen columns">
                <h1>Meskipun kami sedikit namun <strong class="color">efektif</strong>.</h1>
            </div>
            <div class=" columns">
                <h4><strong>Filosofi Kami.</strong></h4>
                <p>Setiap individu pasti meliliki cinta. Dalam konteks ini cinta yang dimaksud adalah kecintaan terhadap sesuatau (passion). Kecintaan ini idealnya bisa menjadi ruh potensi dan kreativitas mahasiswa yang akan didesikasikan untuk kemajuan Amikom dan Indonesia. Namun sekarang potensi dan kreatifitas ini belum bisa menjadi wujud kontibusi. Maka dari itu kita ingin menghadirkan BEM KM sebagai wadah yang bisa merangkai kecintaan tersebut.Karena cinta – cinta ini ketika dirangkai akan menjadi energi yang sangat besar dalam melandasi karya-karya besar ke depan.</p>
                <p>Sebuah karya akan besar jika dilandasi dengan cinta. Cinta ibarat sebuah energi yang akan mengarahkan kemanakah karya ini akan diberikan..</p>
            </div>
            
            <div class="sixteen columns"><hr class="spacer"></div>
        </div>

        <div class="row">
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon bulb"></span>
                    <h4>Ide cemerlang.</h4>
                    <p>Kami adalah orang-orang dengan ide yang cemerlang.</p>
                </div>
            </div>
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon gauge"></span>
                    <h4>Cepat tanggap.</h4>
                    <p>Ketika yang lain menunggu, kami maju.</p>
                </div>
            </div>
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon support"></span>
                    <h4>Pendukung kuat.</h4>
                    <p>Mahasiswa adalah para aktifis yang selalu kami dukung.</p>
                </div>
            </div>
            <div class="four columns">
                <div class="featured-box-2">
                    <span class="icon cycle"></span>
                    <h4>Membantu sesama.</h4>
                    <p>Tanpa mengenal lelah kami bantu sesama.</p>
                </div>
            </div>
        </div>

    </div>
</div>