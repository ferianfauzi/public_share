<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <title>BEM AMIKOM</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Official Website of BEM Amikom">
        <meta name="author" content="BEM AMIKOM">

        <!-- The styles -->
        <link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <link href="css/charisma-app.css" rel="stylesheet">
        <link href="css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='css/fullcalendar.css' rel='stylesheet'>
        <link href='css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='css/chosen.css' rel='stylesheet'>
        <link href='css/uniform.default.css' rel='stylesheet'>
        <link href='css/colorbox.css' rel='stylesheet'>
        <link href='css/jquery.cleditor.css' rel='stylesheet'>
        <link href='css/jquery.noty.css' rel='stylesheet'>
        <link href='css/noty_theme_default.css' rel='stylesheet'>
        <link href='css/elfinder.min.css' rel='stylesheet'>
        <link href='css/elfinder.theme.css' rel='stylesheet'>
        <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='css/opa-icons.css' rel='stylesheet'>
        <link href='css/uploadify.css' rel='stylesheet'>
        <script>
            function goBack() {
                window.history.back()
            }
        </script>
        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="img/favicon.ico">

    </head>

    <body>
        <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
            <!-- topbar starts -->
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container-fluid">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="media.php?module=home"> <img alt="amikom Logo" src="img/logo20.png" /> <span>AMIKOM</span></a>

                        <!-- theme selector starts -->
                        <div class="btn-group pull-right theme-container" >
                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="icon-tint"></i><span class="hidden-phone"> Ganti Tema </span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" id="themes">
                                <li><a data-value="classic" href="#"><i class="icon-blank"></i> Classic</a></li>
                                <li><a data-value="cerulean" href="#"><i class="icon-blank"></i> Cerulean</a></li>
                                <li><a data-value="cyborg" href="#"><i class="icon-blank"></i> Cyborg</a></li>
                                <li><a data-value="redy" href="#"><i class="icon-blank"></i> Redy</a></li>
                                <li><a data-value="journal" href="#"><i class="icon-blank"></i> Journal</a></li>
                                <li><a data-value="simplex" href="#"><i class="icon-blank"></i> Simplex</a></li>
                                <li><a data-value="slate" href="#"><i class="icon-blank"></i> Slate</a></li>
                                <li><a data-value="spacelab" href="#"><i class="icon-blank"></i> Spacelab</a></li>
                                <li><a data-value="united" href="#"><i class="icon-blank"></i> United</a></li>
                            </ul>
                        </div>
                        <!-- theme selector ends -->

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right" >
                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="icon-user"></i><span class="hidden-phone"> admin</span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="logout.php">Logout</a></li>
                            </ul>
                        </div>
                        <!-- user dropdown ends -->

                        <div class="top-nav nav-collapse">
                            <ul class="nav">
                                <li><a href="#">Kunjungi Web</a></li>
                                <li>
                                    <form class="navbar-search pull-left">
                                        <input placeholder="Search" class="search-query span2" name="query" type="text">
                                    </form>
                                </li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- topbar ends -->
        <?php } ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>

                    <!-- left menu starts -->
                    <div class="span2 main-menu-span">
                        <div class="well nav-collapse sidebar-nav">
                            <ul class="nav nav-tabs nav-stacked main-menu">
                                <li class="nav-header hidden-tablet">Manajemen User</li>
                                <li><a class="ajax-link" href="media.php?module=admin"><i class="icon-home"></i><span class="hidden-tablet"> Admin</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=anggota"><i class="icon-eye-open"></i><span class="hidden-tablet"> Anggota</span></a></li>
                                <li class="nav-header hidden-tablet">Manajemen Data</li>
                                <li><a class="ajax-link" href="media.php?module=divisi"><i class="icon-align-justify"></i><span class="hidden-tablet"> Divisi Kerja</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=album"><i class="icon-calendar"></i><span class="hidden-tablet"> Album Galeri</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=galeri"><i class="icon-th"></i><span class="hidden-tablet"> Galeri</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=aspirasi"><i class="icon-folder-open"></i><span class="hidden-tablet"> Pesan n Aspirasi</span></a></li>
                                <li class="nav-header hidden-tablet">Manajemen Informasi</li>
                                <li><a class="ajax-link" href="media.php?module=profil"><i class="icon-eye-open"></i><span class="hidden-tablet"> Profil</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=agenda"><i class="icon-edit"></i><span class="hidden-tablet"> Agenda</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=datastatis"><i class="icon-align-justify"></i><span class="hidden-tablet"> Datastatis</span></a></li>
                                <li><a class="ajax-link" href="media.php?module=kontak"><i class="icon-align-left"></i><span class="hidden-tablet"> Kontak</span></a></li>
                            </ul>
                            <label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
                        </div><!--/.well -->
                    </div><!--/span-->
                    <!-- left menu ends -->

                    <noscript>
                    <div class="alert alert-block span10">
                        <h4 class="alert-heading">Warning!</h4>
                        <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                    </div>
                    </noscript>

                    <div id="content" class="span10">

                        <?php
                        $hal = (isset($_GET['module'])) ? $_GET['module'] : 'home';
                        pesan($hal);
                        ?>

                        <!-- content starts -->

                    <?php }
                    ?>
