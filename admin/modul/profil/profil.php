<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/profil/aksi_profil.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from profil where id_profil='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_profil'];
                $tahun_berdiri = $result['tahun_berdiri'];
                $sejarah = $result['sejarah'];
                $nama = $result['nama'];
                $deskripsi = $result['deskripsi'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = "";
                $tahun_berdiri = "";
                $sejarah = "";
                $nama = "";
                $deskripsi = "";
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Pesan</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=profil&act=<?php echo $aksi; ?>" method=post id="frmmodul">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Nama</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="nama" name="nama" type="text" value="<?php echo $nama; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Sejarah</label>
                                    <div class="controls">
                                        <textarea rows="8" class="input-xlarge focused" id="sejarah" name="sejarah" type="text" required=""><?php echo $sejarah; ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Deskripi</label>
                                    <div class="controls">
                                        <textarea rows="8" class="input-xlarge focused" id="deskripsi" name="deskripsi" type="text" required=""><?php echo $deskripsi; ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Tahun Berdiri</label>
                                    <div class="controls">
                                       <input class="input-xlarge focused" id="tahun_berdiri" name="tahun_berdiri" type="text" value="<?php echo $tahun_berdiri; ?>" required="">
                                   
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack()" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM profil");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Pesan</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=profil&act=editor"><i class="icon-plus-sign"></i> Tambah Pesan</a>
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Pesan</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>


                                    <th>Tahun Berdiri</th>
                                    <th>Nama</th>
                                    <th>Sejarah</th>
                                    <th>Deskripsi</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['tahun_berdiri']; ?></td>
                                        <td><?php echo $value['nama']; ?></td>
                                        <td><?php echo substr($value['sejarah'],0,50); ?>...</td>
                                        <td><?php echo substr($value['deskripsi'],0,50); ?>...</td>
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=profil&act=editor&id=<?php echo $value['id_profil']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?')" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_profil']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php } ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>