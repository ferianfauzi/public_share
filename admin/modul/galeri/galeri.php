<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/galeri/aksi_galeri.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from galeri where id_galeri='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_galeri'];
                $foto = $result['foto'];
                $id_album = $result['id_album'];
                $keterangan = $result['keterangan'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = '';
              
                $foto = '';
                $id_album = '';
                $keterangan='';
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Galeri</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=galeri&act=<?php echo $aksi; ?>" method=post id="frmmodul" enctype="multipart/form-data">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Ket Galeri</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="keterangan" name="keterangan" type="text" value="<?php echo $keterangan; ?>" required="">
                                    </div>
                                </div>
                             
                                <div class = "control-group">
                                    <label class = "control-label" for = "focusedInput">Foto</label>
                                    <div class = "controls">
                                        <?php
                                        if ($aksi == "update") {
                                            echo' <img src="upload/thumbs/galeri/' . $foto . '" height=106px width=auto><br>
                                <input type="hidden" name="oldgambar" value="' . $foto . '">';
                                        }
                                        ?>
                                        <input type = file name = 'gambar' value = "<?php echo $foto; ?>" id = "gambar" <?php
                                        if ($aksi == 'insert') {

                                            echo "class='validate[required]'";
                                        }
                                        ?>>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput"> Album</label>
                                    <div class="controls">
                                        <select name="id_album" class="input-xlarge">
                                            <?php
                                            $qy = "select * from album";
                                            $hi = mysql_query($qy) or die(mysql_error() . "[" . $qy . "]");
                                            while ($row = mysql_fetch_array($hi)) {
                                                if ($id_album == $row['id_album']) {
                                                    echo "<option selected value='" . $row['id_album'] . "'>" . $row['nama_album'] . "</option>";
                                                } else {
                                                    echo "<option value='" . $row['id_album'] . "'>" . $row['nama_album'] . "</option>";
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack();" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div>

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM galeri g join album a on g.id_album=a.id_album order by g.id_album");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Galeri</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=galeri&act=editor"><i class="icon-plus-sign"></i> Tambah Galeri</a>
                <!--<a class="btn" target="_blank" href="modul/galeri/cetak_galeri.php"><i class="icon-print"></i> Print Daftar Galeri</a>-->
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Galeri</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>Keterangan</th>
                                    <th>Album</th>
                                    <th>Foto</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['keterangan']; ?></td>
                                        <td><?php echo $value['nama_album']; ?></td>
                                        <td><img height="80" src="upload/thumbs/galeri/<?php echo $value['foto']; ?>"></td>
                                        
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=galeri&act=editor&id=<?php echo $value['id_galeri']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?')" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_galeri']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php } ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>