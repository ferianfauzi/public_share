<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/agenda/aksi_agenda.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from agenda where id_agenda='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_agenda'];
                $tanggal = $result['tanggal'];
                $tujuan = $result['tujuan'];
                $penanggung_jawab = $result['penanggung_jawab'];
                $nama_agenda = $result['nama_agenda'];
                $tempat = $result['tempat'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = "";
                $tujuan = "";
                $tanggal = "";
                $penanggung_jawab = "";
                $nama_agenda = "";
                $tempat = "";
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Agenda</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=agenda&act=<?php echo $aksi; ?>" method=post id="frmmodul">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Nama Agenda</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="nama_agenda" name="nama_agenda" type="text" value="<?php echo $nama_agenda; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Tanggal Agenda</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused datepicker" id="datepicker" name="tanggal" type="text" value="<?php echo $tanggal; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Tujuan Agenda</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="tujuan" name="tujuan" type="text" value="<?php echo $tujuan; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Penanggung Jawab</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="penanggung_jawab" name="penanggung_jawab" type="text" value="<?php echo $penanggung_jawab; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Tempat</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="tempat" name="tempat" type="text" required="" value="<?php echo $tempat; ?>">
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack()" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM agenda");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Agenda</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=agenda&act=editor"><i class="icon-plus-sign"></i> Tambah Agenda</a>
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Agenda</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>

                                    <th>Agenda</th>
                                    <th>Tempat</th>
                                    <th>Tujuan</th>
                                    <th>Tanggal</th>
                                    <th>Penanggung Jawab</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                $no = 1;
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value['nama_agenda']; ?></td>
                                        <td class="center"><?php echo $value['tempat']; ?></td>
                                        <td><?php echo $value['tujuan']; ?></td>
                                        <td><?php echo $value['tanggal']; ?></td>
                                        <td><?php echo $value['penanggung_jawab']; ?></td>
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=agenda&act=editor&id=<?php echo $value['id_agenda']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?')" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_agenda']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php
                    $no++;
                }
                                ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>