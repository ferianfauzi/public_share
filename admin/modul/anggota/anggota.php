<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/anggota/aksi_anggota.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from anggota where id_anggota='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_anggota'];
                $nama = $result['nama_anggota'];
                $alamat = $result['alamat'];
                $foto = $result['foto'];
                $id_devisi = $result['id_devisi'];
                $no_telp = $result['no_telp'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = '';
                $nama = '';
                $alamat = '';
                $foto = '';
                $id_devisi = '';
                $no_telp = '';
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Anggota</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=anggota&act=<?php echo $aksi; ?>" method=post id="frmmodul" enctype="multipart/form-data">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Nama Anggota</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="nama" name="nama" type="text" value="<?php echo $nama; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Alamat</label>
                                    <div class="controls">
                                        <textarea class="input-xlarge focused" id="alamat" name="alamat" type="text" required=""><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">No Telp</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id=no_telp name="no_telp" type="text" value="<?php echo $no_telp; ?>" required="">
                                    </div>
                                </div>
                                <div class = "control-group">
                                    <label class = "control-label" for = "focusedInput">Foto</label>
                                    <div class = "controls">
                                        <?php
                                        if ($aksi == "update") {
                                            echo' <img src="upload/thumbs/anggota/' . $foto . '" height=106px width=auto><br>
                                <input type="hidden" name="oldgambar" value="' . $foto . '">';
                                        }
                                        ?>
                                        <input type = file name = 'gambar' value = "<?php echo $foto; ?>" id = "gambar" <?php
                                        if ($aksi == 'insert') {

                                            echo "class='validate[required]'";
                                        }
                                        ?>>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput"> Divisi</label>
                                    <div class="controls">
                                        <select name="id_devisi" class="input-xlarge">
                                            <?php
                                            $qy = "select id_devisi, nama_devisi from devisi";
                                            $hi = mysql_query($qy) or die(mysql_error() . "[" . $qy . "]");
                                            while ($row = mysql_fetch_array($hi)) {
                                                if ($id_devisi == $row['id_devisi']) {
                                                    echo "<option selected value='" . $row['id_devisi'] . "'>" . $row['nama_devisi'] . "</option>";
                                                } else {
                                                    echo "<option value='" . $row['id_devisi'] . "'>" . $row['nama_devisi'] . "</option>";
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack()" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div>

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM anggota");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Anggota</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=anggota&act=editor"><i class="icon-plus-sign"></i> Tambah Anggota</a>
                <!--<a class="btn" target="_blank" href="modul/anggota/cetak_anggota.php"><i class="icon-print"></i> Print Daftar Anggota</a>-->
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Anggota</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>HP</th>

                                    <th>Alamat</th>
                                   
                                    <th>Foto</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['nama_anggota']; ?></td>

                                        <td><?php echo $value['no_telp']; ?></td>
                                        <td><?php echo $value['alamat']; ?></td>
                                        <td><img height="80" src="upload/thumbs/anggota/<?php echo $value['foto']; ?>"></td>
                                        
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=anggota&act=editor&id=<?php echo $value['id_anggota']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?')" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_anggota']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php } ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>