<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/divisi/aksi_divisi.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from devisi where id_devisi='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_devisi'];
               
                $nama_devisi = $result['nama_devisi'];
                $tugas = $result['tugas'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = "";
               
                $nama_devisi = "";
                $tugas = "";
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Devisi</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=divisi&act=<?php echo $aksi; ?>" method=post id="frmmodul">
                            <fieldset>
                               
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Nama Devisi</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="nama_devisi" name="nama_devisi" type="text" value="<?php echo $nama_devisi; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Tugas</label>
                                    <div class="controls">
                                        <textarea class="input-xlarge focused" id="tugas" name="tugas" type="text" required="" ><?php echo $tugas; ?></textarea>
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack()" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM devisi");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Devisi</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=divisi&act=editor"><i class="icon-plus-sign"></i> Tambah Devisi</a>
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Devisi</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>

                                    <th>Nama Divisi</th>
                                    <th>Tugas</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                $no=1;
                                while ($value = mysql_fetch_array($query)) {
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value['nama_devisi']; ?></td>
                                        <td class="center"><?php echo $value['tugas']; ?></td>
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=divisi&act=editor&id=<?php echo $value['id_devisi']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?')" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_devisi']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php 
                                    $no++;
                                    
                                } ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>