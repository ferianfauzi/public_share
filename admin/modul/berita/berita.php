<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/berita/aksi_berita.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from berita where id_berita='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id_berita = $result['id_berita'];
                $judul = $result['judul'];
                $isi = $result['isi'];
                $foto = $result['foto'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id_berita = "";
                $judul = "";
                $isi = "";
                $foto = "";
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Berita</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=berita&act=<?php echo $aksi; ?>" method=post id="frmmodul"  enctype="multipart/form-data">
                            <fieldset>
                               
                                <div class = "control-group">
                                    <label class = "control-label" for = "focusedInput"> Judul</label>
                                    <div class = "controls">
                                        <input class = "input-xlarge focused" id = "judul" name = "judul" type = "text" value = "<?php echo $judul; ?>" required = "">
                                    </div>
                                </div>
                                
                                <div class = "control-group">
                                    <label class = "control-label" for = "focusedInput"> Isi Berita</label>
                                    <div class = "controls">
                                        <textarea rows="20" cols="40" class = "input-xlarge focused" id = "isi" name = "isi" type = "text" required = ""><?php echo $isi; ?></textarea>
                                    </div>
                                </div>
                              

                                <div class = "control-group">
                                    <label class = "control-label" for = "focusedInput">Gambar </label>
                                    <div class = "controls">
                                        <?php
                                        if ($aksi == "update") {
                                            echo' <img src="upload/thumbs/berita/' . $foto . '" height=106px width=auto><br>
                                <input type="hidden" name="oldgambar" value="' . $foto . '">';
                                        }
                                        ?>
                                        <input type =file name = 'gambar' value = "<?php echo $foto; ?>" id = "gambar" <?php
                                        if ($aksi == 'insert') {

                                            echo "class='validate[required]'";
                                        }
                                        ?>>
                                    </div>
                                </div>


                                <input name="id_berita" class="hidden"value="<?php echo $id_berita; ?>">
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack();" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM berita");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Berita</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=berita&act=editor"><i class="icon-plus-sign"></i> Tambah  Berita</a>
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Berita</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>


                                    <th>Judul</th>
                                    <th>Isi</th>
                                    <th>Tanggal</th>
                                    
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['judul']; ?></td>
                                        <td><?php echo $value['isi']; ?></td>
                                        <td><?php echo $value['tanggal']; ?></td>
                                        
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=berita&act=editor&id=<?php echo $value['id_berita']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?');" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_berita']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php } ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>