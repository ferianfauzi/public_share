<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/kontak/aksi_kontak.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from kontak where id_kontak='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_kontak'];

                $email = $result['email'];
                $alamat = $result['alamat'];
                $no_telp = $result['no_telp'];
                $fun_page = $result['fun_page'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = "";
                $email = "";
                $alamat = "";
                $no_telp = "";
                $fun_page = "";
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Kontak</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=kontak&act=<?php echo $aksi; ?>" method=post id="frmmodul">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Email</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="email" name="email" type="text" value="<?php echo $email; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Alamat</label>
                                    <div class="controls">
                                        <textarea rows="8" class="input-xlarge focused" id="alamat" name="alamat" type="text" required=""><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">No Telp</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id=no_telp name="no_telp" type="text" value="<?php echo $no_telp; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Alamat Fanpage</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="fun_page" name="fun_page" type="text" value="<?php echo $fun_page; ?>" required="">
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack()" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM kontak");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Kontak</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <a class="btn" href="?module=kontak&act=editor"><i class="icon-plus-sign"></i> Tambah Kontak</a>
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Kontak</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>

                                    <th>Email Kontak</th>
                                    <th>Alamat Kontak</th>
                                    <th>Telp Kontak</th>
                                    <th>FP</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                $no = 1;
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value['email']; ?></td>
                                        <td><?php echo $value['alamat']; ?></td>
                                        <td><?php echo $value['no_telp']; ?></td>
                                        <td><?php echo $value['fun_page']; ?></td>
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=kontak&act=editor&id=<?php echo $value['id_kontak']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?');" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_kontak']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php
                                    $no++;
                                }
                                ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>