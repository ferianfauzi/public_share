<?php
include 'config/cekId.php';
if (empty($_SESSION['username']) AND empty($_SESSION['password'])) {
    echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
    echo "<a href=../../index.php><b>LOGIN</b></a></center>";
} else {
    $link = "modul/aspirasi/aksi_aspirasi.php";
    switch (isset($_GET['act'])) {
        case 'editor':
            if (isset($_GET['id'])) {
                $query = mysql_query("select * from aspirasi where id_aspirasi='$_GET[id]'");
                $result = mysql_fetch_array($query);
                $id = $result['id_aspirasi'];
                $status = $result['status'];
                $isi = $result['isi'];
                $email = $result['email'];
                $jawaban = $result['jawaban'];
                $aksi = "update";
                $readonly = "readonly";
            } else {
                $id = "";
                $status = "";
                $isi = "";
                $email = "";
                $jawaban = "";
                $aksi = "insert";
                $readonly = "";
            }
            ?>

            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i> Formulir Pesan</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>



                    <div class="box-content">

                        <form class="form-horizontal" action="<?php echo $link; ?>?module=aspirasi&act=<?php echo $aksi; ?>" method=post id="frmmodul">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Email</label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" id="email" name="email" type="text" readonly="" value="<?php echo $email; ?>" required="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Isi Pesan</label>
                                    <div class="controls">
                                        <textarea rows="8" class="input-xlarge focused" id="isi" name="isi" type="text" required=""><?php echo $isi; ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Jawab</label>
                                    <div class="controls">
                                        <textarea rows="8" class="input-xlarge focused" id="jawaban" name="jawaban" type="text" required=""><?php echo $jawaban; ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Status Tampil</label>
                                    <div class="controls">
                                        <select name="status">
                                            <?php
                                            if ($status == "aktif") {

                                                echo ' <option value="aktif" selected > Tampil </option>
                                            <option value="non-aktif"> Tidak Tampil </option> ';
                                            } else {
                                                echo ' <option value="aktif"  > Tampil </option>
                                            <option value="non-aktif" selected> Tidak Tampil </option> ';
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <input name="id" class="hidden"value="<?php echo $id; ?>">
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" onclick="goBack()" class="btn">Cancel</button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;

        default:
            $query = mysql_query("SELECT * FROM aspirasi");
            ?>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">Pesan</a>
                    </li>
                </ul>
            </div>


            <div class="btn-group pull-left">
                <!--<a class="btn" href="?module=aspirasi&act=editor"><i class="icon-plus-sign"></i> Tambah Pesan</a>-->
            </div>


            <div class="row-fluid sortable">		
                <div class="box span12">

                    <div class="box-header well" data-original-title>

                        <h2><i class="icon-user"></i> Pesan</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>

                    </div>  
                    <div class="box-content">

                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>


                                    <th>Status</th>
                                    <th>Email</th>
                                    <th>Isi Pesan</th>
                                    <th>Jawaban</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>  

                            <tbody>
                                <?php
                                while ($value = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value['status']; ?></td>
                                        <td><?php echo $value['email']; ?></td>
                                        <td><?php echo $value['isi']; ?></td>
                                        <td><?php echo $value['jawaban']; ?></td>
                                        <td class="center">
                                            <a class="btn btn-info" href="?module=aspirasi&act=editor&id=<?php echo $value['id_aspirasi']; ?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Balas                                            
                                            </a>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus?')" 
                                               class="btn btn-danger" href="<?php echo $link; ?>?module=modul&act=delete&id=<?php echo $value['id_aspirasi']; ?>">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>   <?php } ?>
                            </tbody> 
                        </table>            
                    </div>
                </div><!--/span-->

            </div><!--/row-->

            <?php
            break;
    }
}
?>