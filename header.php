<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en">
    <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
        ================================================== -->
        <title>BEM AMIKOM | STMIK AMIKOM Yogyakarta</title>
        <meta charset="utf-8">
        <meta name="description" content="website resmi dari bem stmik amikom yogyakarta.">
        <meta name="keywords" content="amikom,bem amikom, bem.">
        <meta name="author" content="fx.">

        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="lib/css/green.css" type="text/css" media="all">
        <link rel="stylesheet" href="lib/css/queries.css" type="text/css" media="all">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,600,700' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="img/favicon.ico">


        <!-- Javascripts
        ================================================== -->
        <script src="lib/js/jquery.min.js"></script>
        <script src="lib/js/jquery.twitter.js"></script>
        <script src="lib/js/jquery.menu.js"></script>
        <script src="lib/js/jquery.mobilemenu.js"></script>
        <script src="lib/js/jquery.fancybox.js"></script>
        <script src="lib/js/jquery.quicksand.js"></script>
        <script src="lib/js/jquery.tabs.js"></script>
        <script src="lib/js/jquery.flickr.js"></script>
        <script src="lib/js/jquery.easing.js"></script>
        <script src="lib/js/jquery.nivoslider.js"></script>
        <script src="lib/js/jquery.touchcarousel.js"></script>
        <script src="lib/js/jquery.contact.js"></script>
        <script src="lib/js/jquery.custom.js"></script>
    </head>

    <!-- Start Document and Body Pattern -->
    <body class="pat9">

        <!-- Boxed Wrapper -->
        <div id="wrapper" class="boxed">

            <!-- Header -->
            <div id="header">
                <!-- Header Top -->
                <div class="container hTop">
                    <div class="row">
                        <div class="sixteen columns">
                            <p>Selamat datang di halaman resmi BEM <a href="http://amikom.ac.id">STMIK AMIKOM Yogyakarta</a></p>
                        </div>
                    </div>
                </div>
                <hr>    
                <!-- Logo and search bar-->
                <div class="container hMiddle">
                    <div class="row">
                        <div class="twelve columns">
                            <div id="logo">
                                <a href="index.php"><img src="img/logo.png" alt="BEM AMIKOM"></a>
                                <p>Laman Resmi BEM AMIKOM Yogyakarta.</p>
                            </div>
                        </div>
                        <div class="four columns">
                            <!--<form class="hSearchbox">
                                <input type="text" placeholder="Search here..." class="hSearchInput"/>
                                <input type="submit" value="Send" class="hSearchSubmit"/>
                            </form>-->
                        </div>
                    </div>
                </div> 
                <hr>       
                <!-- Navigation -->
                <div class="container hBottom">
                    <div class="row">
                        <div class="sixteen columns">
                            <ul id="menu">

                                <li><a href="index.php">Home</a>

                                </li>

                                <li><a href="#">Tentang Kami</a>
                                    <ul>
                                        <?php
                                        $kueri = "select * from datastatis";
                                        $run = mysql_query($kueri);
                                        while ($row = mysql_fetch_array($run)) {
                                            echo '<li><a href="index.php?menu=datastatis&id=' . $row['id_datastatis'] . '">' . $row['nama'] . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                </li>

                                <li><a href="#">Profil</a>
                                    <ul>
                                        <?php
                                        $kueri = "select id_profil,nama from profil";
                                        $run = mysql_query($kueri);
                                        while ($row = mysql_fetch_array($run)) {
                                            echo '<li><a href="index.php?menu=profil&id=' . $row['id_profil'] . '">' . $row['nama'] . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li><a href="#">Divisi dan Anggota</a>
                                    <ul>
                                        <?php
                                        $kue = "select id_devisi,nama_devisi from devisi";
                                        $runit = mysql_query($kue);
                                        while ($row = mysql_fetch_array($runit)) {
                                            echo '<li><a href="index.php?menu=divisi&id=' . $row['id_devisi'] . '">' . $row['nama_devisi'] . '</a></li>';
                                        }
                                        ?>

                                    </ul>
                                </li>
                                <li><a href="index.php?menu=agenda">Agenda</a></li>
                                <li><a href="index.php?menu=galeri">Galeri</a></li>
                                <li><a href="index.php?menu=aspirasi">Aspirasi</a></li>
                                <li><a href="index.php?menu=kontak">Kontak</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
